import * as consoleOas from "@latency.gg/lgg-console-oas";
import * as application from "../application/index.js";
import { selectClientSnapshot } from "../queries/index.js";
import { mapQueryEvents } from "../utils/index.js";

export function createSubscribeClientEventsOperation(
    context: application.Context,
): consoleOas.SubscribeClientEventsOperationHandler<application.Authorization> {
    return async function (incomingRequest, authorization) {
        const { user } = authorization.accessToken;

        return {
            status: 200,
            parameters: {},
            async * entities(signal) {
                const queryPromise = context.services.authorizedClientQuery.acquire(user);
                try {
                    const query = await queryPromise;
                    const state = query.getState();
                    yield selectClientSnapshot(state);
                    yield* mapQueryEvents(query.fork(signal));
                }
                catch (error) {
                    if (!signal?.aborted) throw error;
                }
                finally {
                    context.services.authorizedClientQuery.release(
                        queryPromise,
                        context.config.linger,
                    );
                }
            },
        };
    };
}

