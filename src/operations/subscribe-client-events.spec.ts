import * as authOas from "@latency.gg/lgg-auth-oas";
import { isAbortError } from "abort-tools";
import assert from "assert";
import { BufferedIterable, createBufferedIterable } from "buffered-iterable";
import delay from "delay";
import { createIterableFanout } from "iterable-fanout";
import { second } from "msecs";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("subscribe-client-events", t => withContext(error => t.error(error), async context => {
    context.servers.consoleApi.registerAccessTokenAuthorization(credential => {
        return {
            provider: "local",
            subject: credential,
            user: credential,
        };
    });

    const authClientEvents = createBufferedIterable<
        authOas.SubscribeClientEvents200ApplicationJsonResponseSchema
    >();
    context.servers.authApi.registerSubscribeClientEventsOperation(incomingRequest => {
        return {
            status: 200,
            parameters: {},
            entities(signal) {
                return authClientEvents;
            },
        };
    });

    const authClientOwnershipEventsA = createBufferedIterable<
        authOas.SubscribeClientOwnershipEvents200ApplicationJsonResponseSchema
    >();
    const authClientOwnershipEventsB = createBufferedIterable<
        authOas.SubscribeClientOwnershipEvents200ApplicationJsonResponseSchema
    >();
    context.servers.authApi.registerSubscribeClientOwnershipEventsOperation(incomingRequest => {
        const { user } = incomingRequest.parameters;
        switch (user) {
            case "A0==":
                return {
                    status: 200,
                    parameters: {},
                    entities(signal) {
                        return authClientOwnershipEventsA;
                    },
                };

            case "B0==":
                return {
                    status: 200,
                    parameters: {},
                    entities(signal) {
                        return authClientOwnershipEventsB;
                    },
                };

            default: assert.fail();
        }
    });

    const consoleApiClientA = context.createConsoleApiClient({
        accessToken: "A0==",
    });
    const consoleApiClientB = context.createConsoleApiClient({
        accessToken: "B0==",
    });

    const subscribeClientEventsResultA = await consoleApiClientA.subscribeClientEvents({
        parameters: {},
    });
    assert(subscribeClientEventsResultA.status === 200);

    const subscribeClientEventsResultB = await consoleApiClientB.subscribeClientEvents({
        parameters: {},
    });
    assert(subscribeClientEventsResultB.status === 200);

    const abortController = new AbortController();

    const iterableA = subscribeClientEventsResultA.entities(abortController.signal);
    const iteratorA = iterableA[Symbol.asyncIterator]();

    const iterableB = subscribeClientEventsResultB.entities(abortController.signal);
    const iteratorB = iterableB[Symbol.asyncIterator]();

    try {
        {
            authClientEvents.push({
                type: "client-snapshot",
                payload: {
                    clients: [
                        {
                            created: "2022-04-21T16:20:15Z",
                            id: "10==",
                            name: "10",
                        },
                        {
                            created: "2022-04-21T16:20:15Z",
                            id: "20==",
                            name: "20",
                        },
                    ],
                },
            });

            authClientOwnershipEventsA.push({
                type: "client-ownership-snapshot",
                payload: {
                    clientOwnerships: [
                        {
                            created: "2022-04-21T16:24:05Z",
                            client: "10==",
                        },
                        {
                            created: "2022-04-21T16:24:05Z",
                            client: "30==",
                        },
                    ],
                },
            });

            authClientOwnershipEventsB.push({
                type: "client-ownership-snapshot",
                payload: {
                    clientOwnerships: [
                        {
                            created: "2022-04-21T16:24:05Z",
                            client: "20==",
                        },
                    ],
                },
            });

            const nextA = await iteratorA.next();
            assert(!nextA.done);
            assert(nextA.value.type === "client-snapshot");
            t.deepEqual(
                nextA.value.payload.clients,
                [
                    {
                        created: "2022-04-21T16:20:15Z",
                        id: "10==",
                        name: "10",
                    },
                ],
            );

            const nextB = await iteratorB.next();
            assert(!nextB.done);
            assert(nextB.value.type === "client-snapshot");
            t.deepEqual(
                nextB.value.payload.clients,
                [
                    {
                        created: "2022-04-21T16:20:15Z",
                        id: "20==",
                        name: "20",
                    },
                ],
            );
        }

        {
            authClientOwnershipEventsB.push({
                type: "client-ownership-created",
                payload: {
                    clientOwnership: {
                        created: "2022-04-21T16:51:10Z",
                        client: "10==",
                    },
                },
            });

            const nextB = await iteratorB.next();
            assert(!nextB.done);
            assert(nextB.value.type === "client-created");
            t.deepEqual(
                nextB.value.payload.client,
                {
                    created: "2022-04-21T16:20:15Z",
                    id: "10==",
                    name: "10",
                },
            );
        }

        {
            authClientEvents.push({
                type: "client-updated",
                payload: {
                    client: {
                        created: "2022-04-21T16:51:10Z",
                        id: "10==",
                        name: "10!",
                    },
                },
            });

            const nextA = await iteratorA.next();
            assert(!nextA.done);
            assert(nextA.value.type === "client-updated");
            t.deepEqual(
                nextA.value.payload.client,
                {
                    created: "2022-04-21T16:51:10Z",
                    id: "10==",
                    name: "10!",
                },
            );

            const nextB = await iteratorB.next();
            assert(!nextB.done);
            assert(nextB.value.type === "client-updated");
            t.deepEqual(
                nextB.value.payload.client,
                {
                    created: "2022-04-21T16:51:10Z",
                    id: "10==",
                    name: "10!",
                },
            );
        }

        {
            authClientEvents.push({
                type: "client-created",
                payload: {
                    client: {
                        created: "2022-04-21T16:54:40Z",
                        id: "30==",
                        name: "30",
                    },
                },
            });

            const nextA = await iteratorA.next();
            assert(!nextA.done);
            assert(nextA.value.type === "client-created");
            t.deepEqual(
                nextA.value.payload.client,
                {
                    created: "2022-04-21T16:54:40Z",
                    id: "30==",
                    name: "30",
                },
            );
        }

        {
            authClientEvents.push({
                type: "client-deleted",
                payload: {
                    client: {
                        created: "2022-04-21T16:20:15Z",
                        id: "20==",
                        name: "20",
                    },
                },
            });

            const nextB = await iteratorB.next();
            assert(!nextB.done);
            assert(nextB.value.type === "client-deleted");
            t.deepEqual(
                nextB.value.payload.client,
                {
                    created: "2022-04-21T16:20:15Z",
                    id: "20==",
                    name: "20",
                },
            );
        }

        {
            authClientEvents.push({
                type: "client-deleted",
                payload: {
                    client: {
                        created: "2022-04-21T16:51:10Z",
                        id: "10==",
                        name: "10!",
                    },
                },
            });

            const nextA = await iteratorA.next();
            assert(!nextA.done);
            assert(nextA.value.type === "client-deleted");
            t.deepEqual(
                nextA.value.payload.client,
                {
                    created: "2022-04-21T16:51:10Z",
                    id: "10==",
                    name: "10!",
                },
            );

            const nextB = await iteratorB.next();
            assert(!nextB.done);
            assert(nextB.value.type === "client-deleted");
            t.deepEqual(
                nextB.value.payload.client,
                {
                    created: "2022-04-21T16:51:10Z",
                    id: "10==",
                    name: "10!",
                },
            );
        }

        {
            authClientOwnershipEventsA.push({
                type: "client-ownership-deleted",
                payload: {
                    clientOwnership: {
                        created: "2022-04-21T16:24:05Z",
                        client: "10==",
                    },
                },
            });
        }

        {
            authClientOwnershipEventsB.push({
                type: "client-ownership-deleted",
                payload: {
                    clientOwnership: {
                        created: "2022-04-21T16:51:10Z",
                        client: "10==",
                    },
                },
            });
        }

        {
            authClientOwnershipEventsA.push({
                type: "client-ownership-deleted",
                payload: {
                    clientOwnership: {
                        created: "2022-04-21T16:24:05Z",
                        client: "30==",
                    },
                },
            });

            const nextA = await iteratorA.next();
            assert(!nextA.done);
            assert(nextA.value.type === "client-deleted");
            t.deepEqual(
                nextA.value.payload.client,
                {
                    created: "2022-04-21T16:54:40Z",
                    id: "30==",
                    name: "30",
                },
            );
        }

        {
            authClientEvents.push({
                type: "client-deleted",
                payload: {
                    client: {
                        created: "2022-04-21T16:54:40Z",
                        id: "30==",
                        name: "30",
                    },
                },
            });
        }
    }
    catch (error) {
        if (!abortController.signal.aborted) throw error;
    }
    finally {
        abortController.abort();

        authClientEvents.done();
        authClientOwnershipEventsA.done();
        authClientOwnershipEventsB.done();

        await Promise.all([
            iteratorA.next().
                then(
                    () => t.fail(),
                    error => t.ok(isAbortError(error)),
                ),
            iteratorB.next().
                then(
                    () => t.fail(),
                    error => t.ok(isAbortError(error)),
                ),
        ]);
    }

}));

test("subscribe-client-events unhappy", t => withContext(error => t.ok(error, "error is ok"), async context => {
    let forked = 0;

    context.servers.consoleApi.registerAccessTokenAuthorization(credential => {
        return {
            provider: "local",
            subject: credential,
            user: credential,
        };
    });

    const authClientEvents: BufferedIterable<
        authOas.SubscribeClientEvents200ApplicationJsonResponseSchema
    > = createBufferedIterable();
    const authClientOwnershipEventsA: BufferedIterable<
        authOas.SubscribeClientOwnershipEvents200ApplicationJsonResponseSchema
    > = createBufferedIterable();
    const authClientOwnershipEventsB: BufferedIterable<
        authOas.SubscribeClientOwnershipEvents200ApplicationJsonResponseSchema
    > = createBufferedIterable();

    const authClientEventsFanout = createIterableFanout(authClientEvents);
    const authClientOwnershipEventsAFanout = createIterableFanout(authClientOwnershipEventsA);
    const authClientOwnershipEventsBFanout = createIterableFanout(authClientOwnershipEventsB);

    context.servers.authApi.registerSubscribeClientEventsOperation(incomingRequest => {
        return {
            status: 200,
            parameters: {},
            entities(signal) {
                forked++;
                return authClientEventsFanout.fork(signal);
            },
        };
    });

    context.servers.authApi.registerSubscribeClientOwnershipEventsOperation(incomingRequest => {
        const { user } = incomingRequest.parameters;
        switch (user) {
            case "A0==":
                return {
                    status: 200,
                    parameters: {},
                    entities(signal) {
                        forked++;
                        return authClientOwnershipEventsAFanout.fork(signal);
                    },
                };

            case "B0==":
                return {
                    status: 200,
                    parameters: {},
                    entities(signal) {
                        forked++;
                        return authClientOwnershipEventsBFanout.fork(signal);
                    },
                };

            default: assert.fail();
        }
    });

    const consoleApiClientA = context.createConsoleApiClient({
        accessToken: "A0==",
    });
    const consoleApiClientB = context.createConsoleApiClient({
        accessToken: "B0==",
    });

    const subscribeClientEventsResultA = await consoleApiClientA.subscribeClientEvents({
        parameters: {},
    });
    assert(subscribeClientEventsResultA.status === 200);

    const subscribeClientEventsResultB = await consoleApiClientB.subscribeClientEvents({
        parameters: {},
    });
    assert(subscribeClientEventsResultB.status === 200);

    const abortController = new AbortController();

    const iterableA = subscribeClientEventsResultA.entities(abortController.signal);
    const iteratorA = iterableA[Symbol.asyncIterator]();

    const iterableB = subscribeClientEventsResultB.entities(abortController.signal);
    const iteratorB = iterableB[Symbol.asyncIterator]();

    await waitForEqual(
        3,
        () => forked,
    );

    try {
        {
            authClientEvents.push({
                type: "client-snapshot",
                payload: {
                    clients: [
                        {
                            created: "2022-04-21T16:20:15Z",
                            id: "10==",
                            name: "10",
                        },
                        {
                            created: "2022-04-21T16:20:15Z",
                            id: "20==",
                            name: "20",
                        },
                    ],
                },
            });

            authClientOwnershipEventsA.push({
                type: "client-ownership-snapshot",
                payload: {
                    clientOwnerships: [
                        {
                            created: "2022-04-21T16:24:05Z",
                            client: "10==",
                        },
                    ],
                },
            });

            authClientOwnershipEventsB.push({
                type: "client-ownership-snapshot",
                payload: {
                    clientOwnerships: [
                        {
                            created: "2022-04-21T16:24:05Z",
                            client: "20==",
                        },
                    ],
                },
            });

            const nextA = await iteratorA.next();
            assert(!nextA.done);
            assert(nextA.value.type === "client-snapshot");
            t.deepEqual(
                nextA.value.payload.clients,
                [
                    {
                        created: "2022-04-21T16:20:15Z",
                        id: "10==",
                        name: "10",
                    },
                ],
            );

            const nextB = await iteratorB.next();
            assert(!nextB.done);
            assert(nextB.value.type === "client-snapshot");
            t.deepEqual(
                nextB.value.payload.clients,
                [
                    {
                        created: "2022-04-21T16:20:15Z",
                        id: "20==",
                        name: "20",
                    },
                ],
            );

        }

        {
            // same snapshot after initialize should not generate
            // events

            authClientEvents.push({
                type: "client-snapshot",
                payload: {
                    clients: [
                        {
                            created: "2022-04-21T16:20:15Z",
                            id: "10==",
                            name: "10",
                        },
                        {
                            created: "2022-04-21T16:20:15Z",
                            id: "20==",
                            name: "20",
                        },
                    ],
                },
            });

            authClientOwnershipEventsA.push({
                type: "client-ownership-snapshot",
                payload: {
                    clientOwnerships: [
                        {
                            created: "2022-04-21T16:24:05Z",
                            client: "10==",
                        },
                        {
                            created: "2022-04-21T16:24:05Z",
                            client: "30==",
                        },
                    ],
                },
            });

            authClientOwnershipEventsB.push({
                type: "client-ownership-snapshot",
                payload: {
                    clientOwnerships: [
                        {
                            created: "2022-04-21T16:24:05Z",
                            client: "20==",
                        },
                    ],
                },
            });
        }

        {
            // different snapshot after initialize will reset the query

            authClientEvents.push({
                type: "client-snapshot",
                payload: {
                    clients: [
                        {
                            created: "2022-04-21T16:20:15Z",
                            id: "10==",
                            name: "10!",
                        },
                        {
                            created: "2022-04-21T16:20:15Z",
                            id: "20==",
                            name: "20!",
                        },
                    ],
                },
            });
        }

        await waitForEqual(
            6,
            () => forked,
        );

        // now we got a fresh query that we need to hydrate
        {
            authClientEvents.push({
                type: "client-snapshot",
                payload: {
                    clients: [
                        {
                            created: "2022-04-21T16:20:15Z",
                            id: "10==",
                            name: "10!",
                        },
                        {
                            created: "2022-04-21T16:20:15Z",
                            id: "20==",
                            name: "20!",
                        },
                    ],
                },
            });

            authClientOwnershipEventsA.push({
                type: "client-ownership-snapshot",
                payload: {
                    clientOwnerships: [
                        {
                            created: "2022-04-21T16:24:05Z",
                            client: "10==",
                        },
                    ],
                },
            });

            authClientOwnershipEventsB.push({
                type: "client-ownership-snapshot",
                payload: {
                    clientOwnerships: [
                        {
                            created: "2022-04-21T16:24:05Z",
                            client: "20==",
                        },
                    ],
                },
            });

            const nextA = await iteratorA.next();
            assert(!nextA.done);
            assert(nextA.value.type === "client-snapshot");
            t.deepEqual(
                nextA.value.payload.clients,
                [
                    {
                        created: "2022-04-21T16:20:15Z",
                        id: "10==",
                        name: "10!",
                    },
                ],
            );

            const nextB = await iteratorB.next();
            assert(!nextB.done);
            assert(nextB.value.type === "client-snapshot");
            t.deepEqual(
                nextB.value.payload.clients,
                [
                    {
                        created: "2022-04-21T16:20:15Z",
                        id: "20==",
                        name: "20!",
                    },
                ],
            );

        }

    }
    catch (error) {
        if (!abortController.signal.aborted) throw error;
    }
    finally {
        abortController.abort();

        authClientEvents.done();
        authClientOwnershipEventsA.done();
        authClientOwnershipEventsB.done();

        await Promise.all([
            iteratorA.next().
                then(
                    () => t.fail(),
                    error => t.ok(isAbortError(error)),
                ),
            iteratorB.next().
                then(
                    () => t.fail(),
                    error => t.ok(isAbortError(error)),
                ),
        ]);
    }

}));

async function waitForEqual<T>(expect: T, factory: () => T) {
    while (factory() !== expect) {
        await delay(1 * second);
    }
}
