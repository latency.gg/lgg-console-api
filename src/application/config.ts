import { HttpSendReceive } from "@oas3/http-send-receive";
import * as prom from "prom-client";

export interface Config {
    endpoint: URL;

    authApiEndpoint: URL;

    oauthClientId?: string,
    openIdConfiguration?: any,

    abortController: AbortController;
    promRegistry: prom.Registry;

    httpSendReceive?: HttpSendReceive;

    linger: number,
    retryIntervalBase: number,
    retryIntervalCap: number,
}
