import test from "tape-promise/tape.js";
import { withContext } from "./context.js";

test("with-context", t => withContext(error => t.error(error), async context => {
    t.pass();
}));
