import { assertAbortSignal, setupCascadingAbort } from "abort-tools";
import { createBufferedIterable } from "buffered-iterable";
import { QueryFactory, QuerySource, QuerySourceElement } from "./index.js";

type MapSnapshot<
    E,
    F extends QueryFactory[]
    > = (
        states: ExtractStates<F>
    ) => Iterable<E>;

type MapEvents<
    E,
    F extends QueryFactory[]
    > = {
        readonly [P in keyof F]: (
            event: ExtractEvent<F[P]>,
            stateNext: ExtractState<F[P]>,
            states: ExtractStates<F>,
        ) => Iterable<E>
    }

type ExtractState<F> =
    F extends QueryFactory<infer S> ? S : never
type ExtractEvent<F> =
    F extends QueryFactory<any, infer E> ? E : never
type ExtractArg<F> =
    F extends QueryFactory<any, any, infer A> ? A : never

type ExtractStates<
    F extends QueryFactory[]
    > = {
        [P in keyof F]: ExtractState<F[P]>
    }
type ExtractEvents<
    F extends QueryFactory[]
    > = {
        [P in keyof F]: ExtractEvent<F[P]>
    }
type ExtractArgs<
    F extends QueryFactory[]
    > = {
        [P in keyof F]: ExtractArg<F[P]>
    }

export interface JoinEventsConfig<
    E,
    F extends QueryFactory[],
    > {
    signal: AbortSignal
    queryFactories: Readonly<F>,
    mapSnapshot: MapSnapshot<E, F>
    mapEvents: MapEvents<E, F>
}
export async function* createJoinEvents<
    E,
    F extends QueryFactory[],
    >(
        config: JoinEventsConfig<E, F>,
        ...args: ExtractArgs<F>
    ): AsyncIterable<E> {

    const {
        signal,
        queryFactories,
        mapSnapshot,
        mapEvents,
    } = config;

    const queryPromises = queryFactories.map(
        (queryFactory, index) => queryFactory.acquire(...args[index]),
    );

    const abortController = new AbortController();
    setupCascadingAbort(signal, abortController);

    try {
        /*
        setup vars
        */
        let iterables: AsyncIterable<QuerySourceElement<unknown, unknown>>[];
        let states: unknown[];

        /*
        Phase one, the snapshot
        */
        {
            const resolved = await Promise.all(
                queryPromises.map(
                    queryPromise => resolveQuery(queryPromise, abortController.signal),
                ),
            );

            states = resolved.map(([state]) => state);
            iterables = resolved.map(([, iterable]) => iterable);

            for (const event of mapSnapshot(states as ExtractStates<F>)) {
                yield event;
            }
        }

        assertAbortSignal(signal, "join aborted");

        /*
        Phase two, events
        */
        {
            const sink = createBufferedIterable<E>();

            const readQuery = async (index: number) => {
                const mapEvent = mapEvents[index];
                const iterable = iterables[index];
                try {
                    for await (
                        const { event: queryEvent, state: queryStateNext } of iterable
                    ) {
                        for (const event of mapEvent(
                            queryEvent, queryStateNext, states,
                        )) {
                            sink.push(event);
                        }

                        states[index] = queryStateNext;
                    }
                }
                catch (error) {
                    if (!abortController.signal.aborted) throw error;
                }

                sink.done();
            };

            iterables.map((iterable, index) => {
                readQuery(index).
                    catch(error => sink.error(error));
            });

            yield* sink;

            assertAbortSignal(signal, "join aborted");
        }
        /*
        */

    }
    finally {
        abortController.abort();

        queryFactories.map(
            (queryFactory, index) => queryFactory.release(queryPromises[index]),
        );
    }
}

async function resolveQuery<S, E>(
    queryPromise: Promise<QuerySource<S, E>>,
    signal: AbortSignal,
) {
    const query = await queryPromise;
    const state = query.getState();
    const eventIterable = query.fork(signal);
    return [state, eventIterable] as const;
}
