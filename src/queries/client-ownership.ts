import * as authOas from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import deepEqual from "fast-deep-equal";
import immutable from "immutable";
import { InstanceMemoizer } from "instance-memoizer";
import * as application from "../application/index.js";
import { createQueryFactory, ErrorEvent, isErrorEvent, QuerySource } from "../utils/index.js";

//#region query

export type ClientOwnershipQuery = InstanceMemoizer<
    Promise<ClientOwnershipQuerySource>, [string]
>

export type ClientOwnershipQuerySource = QuerySource<
    ClientOwnershipState, ClientOwnershipEventUnion
>

export function createClientOwnershipQueryFactory(
    config: application.Config,
    services: Pick<application.Services, "authApi">,
    onError: (error: unknown) => void,
): ClientOwnershipQuery {
    return createQueryFactory({
        initialState, reduce,
        createSource,
        calculateHash: user => user,
        onError,
        settle: () => true,
        retryIntervalBase: config.retryIntervalBase,
        retryIntervalCap: config.retryIntervalCap,
    });

    async function* createSource(
        signal: AbortSignal,
        user: string,
    ) {
        const result =
            await services.authApi.subscribeClientOwnershipEvents({
                parameters: {
                    user,
                },
            });

        assert(result.status === 200, "expected 200");

        yield* result.entities(signal);
    }
}

//#endregion

//#region state / events

export interface ClientOwnershipEntity {
    created: string
}

export interface ClientOwnershipState {
    error: boolean;
    entities: immutable.Map<string, ClientOwnershipEntity>;
}

const initialState: ClientOwnershipState = {
    error: false,
    entities: immutable.Map(),
};

export type ClientOwnershipEventUnion =
    authOas.SubscribeClientOwnershipEvents200ApplicationJsonResponseSchema;

function reduce(
    state: ClientOwnershipState,
    event: ClientOwnershipEventUnion | ErrorEvent,
): ClientOwnershipState {
    if (isErrorEvent(event)) {
        return {
            ...state,
            error: true,
        };
    }

    switch (event.type) {
        case "client-ownership-snapshot": {
            let { entities } = initialState;

            entities = entities.withMutations(entities => {
                for (
                    const {
                        client,
                        created,
                    } of event.payload.clientOwnerships
                ) {
                    assert(!entities.has(client), "client-ownership already exist");

                    const entity = {
                        created,
                    };
                    entities.set(client, entity);
                }

            });

            return {
                error: false,
                entities,
            };
        }

        case "client-ownership-created": {
            let { entities } = state;

            const {
                client,
                created,
            } = event.payload.clientOwnership;

            assert(!entities.has(client), "client-ownership already exist");

            const entity: ClientOwnershipEntity = {
                created,
            };
            entities = entities.set(client, entity);

            return {
                ...state,
                entities,
            };
        }

        case "client-ownership-deleted": {
            let { entities } = state;

            const {
                client,
            } = event.payload.clientOwnership;

            assert(entities.has(client), "client-ownership does not exist");
            entities = entities.delete(client);

            return {
                ...state,
                entities,
            };
        }

        default: return state;
    }
}

//#endregion

//#region selectors

export function selectClientOwnershipExists(
    state: ClientOwnershipState,
    client: string,
): boolean {
    return state.entities.has(client);
}

export function selectClientOwnershipEntity(
    state: ClientOwnershipState,
    client: string,
): ClientOwnershipEntity | undefined {
    const clientOwnerships = state.entities;
    const entity = clientOwnerships.get(client);
    if (!entity) return;

    return entity;
}

export function selectClientOwnershipEntries(
    state: ClientOwnershipState,
): Iterable<[string, ClientOwnershipEntity]> {
    return state.entities.entries();
}

export function selectClientOwnershipStateEqual(
    state: ClientOwnershipState,
    otherState: ClientOwnershipState,
) {
    if (state.entities.count() !== otherState.entities.count()) {
        return false;
    }

    for (const [key, entity] of state.entities) {
        const otherEntity = otherState.entities.get(key);
        if (!deepEqual(entity, otherEntity)) return false;
    }

    return true;
}

export function selectClientOwnershipStateEmpty(
    state: ClientOwnershipState,
) {
    return state.entities.isEmpty();
}

//#endregion
