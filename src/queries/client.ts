import * as consoleOas from "@latency.gg/lgg-console-oas";
import assert from "assert";
import deepEqual from "fast-deep-equal";
import immutable from "immutable";
import { InstanceMemoizer } from "instance-memoizer";
import * as application from "../application/index.js";
import { createJoinEvents, createQueryFactory, ErrorEvent, isErrorEvent, QuerySource } from "../utils/index.js";
import * as queries from "./index.js";

//#region query

export type ClientQuery = InstanceMemoizer<
    Promise<ClientQuerySource>, []
>

export type ClientQuerySource = QuerySource<
    ClientState, ClientEventUnion
>

export function createClientQueryFactory(
    config: application.Config,
    services: Pick<application.Services, "authApi">,
    onError: (error: unknown) => void,
): ClientQuery {
    return createQueryFactory({
        initialState, reduce,
        createSource,
        calculateHash: () => "",
        onError,
        settle: () => true,
        retryIntervalBase: config.retryIntervalBase,
        retryIntervalCap: config.retryIntervalCap,
    });

    async function* createSource(
        signal: AbortSignal,
    ) {
        const result = await services.authApi.subscribeClientEvents({
            parameters: {},
        });

        assert(result.status === 200, "expected 200");

        yield* result.entities(signal);
    }
}

//#endregion

//#region query (authorized)

export type AuthorizedClientQuery = InstanceMemoizer<
    Promise<AuthorizedClientQuerySource>, [string]
>

export type AuthorizedClientQuerySource = QuerySource<
    ClientState, ClientEventUnion
>;

export function createAuthorizedClientQueryFactory(
    config: application.Config,
    services: Pick<application.Services, "clientQuery" | "clientOwnershipQuery">,
    onError: (error: unknown) => void,
): AuthorizedClientQuery {
    return createQueryFactory({
        initialState, reduce,
        settle: () => true,
        createSource,
        calculateHash: user => user,
        onError,
        retryIntervalBase: config.retryIntervalBase,
        retryIntervalCap: config.retryIntervalCap,
    });

    async function* createSource(
        signal: AbortSignal,
        user: string,
    ): AsyncIterable<ClientEventUnion> {
        yield* createJoinEvents<
            ClientEventUnion,
            [ClientQuery, queries.ClientOwnershipQuery]
        >(
            {
                signal,

                queryFactories: [
                    services.clientQuery,
                    services.clientOwnershipQuery,
                ],

                mapSnapshot,
                mapEvents: [
                    mapClientEvents,
                    mapClientOwnershipEvents,
                ],
            },
            [],
            [user],
        );

        function* mapSnapshot(
            [
                clientState,
                clientOwnershipState,
            ]:
                [
                    ClientState,
                    queries.ClientOwnershipState
                ],
        ): Iterable<ClientEventUnion> {
            const clients = [
                ...selectClientEntries(clientState),
            ].
                map(([id, { name, created }]) => ({ id, name, created })).
                filter(({ id }) => queries.selectClientOwnershipExists(clientOwnershipState, id));

            yield {
                type: "client-snapshot",
                payload: { clients },
            };
        }

        // eslint-disable-next-line require-yield
        function* mapClientEvents(
            clientEvent: ClientEventUnion | ErrorEvent,
            clientStateNext: ClientState,
            [
                clientState,
                clientOwnershipState,
            ]: [
                    ClientState,
                    queries.ClientOwnershipState
                ],
        ): Iterable<ClientEventUnion> {
            if (isErrorEvent(clientEvent)) {
                return;
            }

            switch (clientEvent.type) {
                case "client-snapshot":
                    assert(
                        queries.selectClientStateEqual(
                            clientStateNext,
                            clientState,
                        ),
                        "query reset",
                    );
                    break;

                case "client-created": {
                    const { id } = clientEvent.payload.client;
                    if (
                        !queries.selectClientOwnershipExists(clientOwnershipState, id)
                    ) break;

                    yield clientEvent;
                    break;
                }

                case "client-deleted": {
                    const { id } = clientEvent.payload.client;
                    if (
                        !queries.selectClientOwnershipExists(clientOwnershipState, id)
                    ) break;

                    yield clientEvent;
                    break;
                }

                case "client-updated": {
                    const { id } = clientEvent.payload.client;
                    if (
                        !queries.selectClientOwnershipExists(clientOwnershipState, id)
                    ) break;

                    yield clientEvent;
                    break;
                }
            }
        }

        // eslint-disable-next-line require-yield
        function* mapClientOwnershipEvents(
            clientOwnershipEvent: queries.ClientOwnershipEventUnion | ErrorEvent,
            clientOwnershipStateNext: queries.ClientOwnershipState,
            [
                clientState,
                clientOwnershipState,
            ]: [
                    ClientState,
                    queries.ClientOwnershipState
                ],
        ): Iterable<ClientEventUnion> {
            if (isErrorEvent(clientOwnershipEvent)) {
                return;
            }

            switch (clientOwnershipEvent.type) {
                case "client-ownership-snapshot":
                    assert(
                        queries.selectClientOwnershipStateEqual(
                            clientOwnershipStateNext,
                            clientOwnershipState,
                        ),
                        "query reset",
                    );
                    break;

                case "client-ownership-created": {
                    const { client } = clientOwnershipEvent.payload.clientOwnership;
                    const clientEntity = selectClientEntity(
                        clientState,
                        client,
                    );
                    if (clientEntity == null) {
                        break;
                    }
                    yield {
                        type: "client-created",
                        payload: {
                            client: {
                                id: client,
                                name: clientEntity.name,
                                created: clientEntity.created,
                            },
                        },
                    };
                    break;
                }

                case "client-ownership-deleted": {
                    const { client } = clientOwnershipEvent.payload.clientOwnership;
                    const clientEntity = selectClientEntity(
                        clientState,
                        client,
                    );
                    if (clientEntity == null) {
                        break;
                    }
                    yield {
                        type: "client-deleted",
                        payload: {
                            client: {
                                id: client,
                                name: clientEntity.name,
                                created: clientEntity.created,
                            },
                        },
                    };
                    break;
                }
            }
        }

    }
}

//#endregion

//#region state / events

export interface ClientEntity {
    name: string
    created: string
}

export interface ClientState {
    error: boolean;
    entities: immutable.Map<string, ClientEntity>;
}

const initialState: ClientState = {
    error: false,
    entities: immutable.Map(),
};

export type ClientEventUnion =
    consoleOas.SubscribeClientEvents200ApplicationJsonResponseSchema;

function reduce(
    state: ClientState,
    event: ClientEventUnion | ErrorEvent,
): ClientState {
    if (isErrorEvent(event)) {
        return {
            ...state,
            error: true,
        };
    }

    switch (event.type) {
        case "client-snapshot": {
            let { entities } = initialState;

            entities = entities.withMutations(entities => {
                for (
                    const {
                        id,
                        name,
                        created,
                    } of event.payload.clients
                ) {
                    assert(!entities.has(id), "client already exist");

                    const entity = {
                        name,
                        created,
                    };
                    entities.set(id, entity);
                }
            });

            return {
                error: false,
                entities,
            };
        }

        case "client-created": {
            let { entities } = state;

            const {
                id,
                name,
                created,
            } = event.payload.client;

            assert(!entities.has(id), "client already exist");

            const entity: ClientEntity = {
                name,
                created,
            };
            entities = entities.set(id, entity);

            return {
                ...state,
                entities,
            };
        }

        case "client-updated": {
            let { entities } = state;

            const {
                id,
                name,
                created,
            } = event.payload.client;

            let entity = entities.get(id);
            assert(entity, "client does not exist");
            entity = {
                ...entity,
                name,
                created,
            };

            entities = entities.set(id, entity);

            return {
                ...state,
                entities,
            };
        }

        case "client-deleted": {
            let { entities } = state;

            const {
                id,
            } = event.payload.client;

            assert(entities.has(id), "client does not exist");
            entities = entities.delete(id);

            return {
                ...state,
                entities,
            };
        }

        default: return state;
    }
}

//#endregion

//#region selectors

export function selectClientSnapshot(
    state: ClientState,
): consoleOas.ClientSnapshotSchema {
    const clients = [
        ...state.entities.
            map(({ name, created }, id) => ({ id, name, created })).
            values(),
    ];

    return {
        type: "client-snapshot",
        payload: { clients },
    };
}

export function selectClientExists(
    state: ClientState,
    id: string,
): boolean {
    return state.entities.has(id);
}

export function selectClientEntity(
    state: ClientState,
    id: string,
): ClientEntity | undefined {
    const clients = state.entities;
    const entity = clients.get(id);
    if (!entity) return;

    return entity;
}

export function selectClientEntries(
    state: ClientState,
): Iterable<[string, ClientEntity]> {
    return state.entities.entries();
}

export function selectClientStateEqual(
    state: ClientState,
    otherState: ClientState,
) {
    if (state.entities.count() !== otherState.entities.count()) {
        return false;
    }

    for (const [key, entity] of state.entities) {
        const otherEntity = otherState.entities.get(key);
        if (!deepEqual(entity, otherEntity)) return false;
    }

    return true;
}

export function selectClientStateEmpty(
    state: ClientState,
) {
    return state.entities.isEmpty();
}

//#endregion
